<?php

add_action('wp_enqueue_scripts', 'nur_child_enqueue_styles');

function nur_child_enqueue_styles()
{
    wp_enqueue_style('nur-child-style', get_stylesheet_uri(), array('nur-style'));
}

function my_style_load()
{
    $theme_uri = get_stylesheet_directory_uri();
    wp_register_style('my_theme_style', $theme_uri . '/assets/css/custom.css');
    wp_register_style('swiper-style-css', $theme_uri . '/assets/libs/swiper/swiper.min.css');

    wp_enqueue_style('my_theme_style');
    wp_enqueue_style('swiper-style-css');
}

add_action('wp_enqueue_scripts', 'my_style_load');


use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_register_fields', 'crb_register_custom_fields');
function crb_register_custom_fields()
{

    Container::make('post_meta', 'Add download file')
        ->show_on_post_type("page")
        ->show_on_template("library-page.php")
        ->add_fields(array(
            Field::make('complex', 'add_download_file')->add_fields(array(
                Field::make("file", 'link_document', 'Завантаження файлу')->set_value_type('url'),
                Field::make('image', 'photo', 'Фото документа'),
                Field::make('text', 'title', 'Назва документа'),
                Field::make('textarea', 'description', 'Опис документу'),
                Field::make('text', 'link_btn', 'Назва кнопки'),
            ))
        ));
}

if (!function_exists('carbon_get_post_meta')) {
    function carbon_get_post_meta($id, $name, $type = null)
    {
        return false;
    }
}

if (!function_exists('carbon_get_the_post_meta')) {
    function carbon_get_the_post_meta($name, $type = null)
    {
        return false;
    }
}

if (!function_exists('carbon_get_theme_option')) {
    function carbon_get_theme_option($name, $type = null)
    {
        return false;
    }
}

if (!function_exists('carbon_get_term_meta')) {
    function carbon_get_term_meta($id, $name, $type = null)
    {
        return false;
    }
}

if (!function_exists('carbon_get_user_meta')) {
    function carbon_get_user_meta($id, $name, $type = null)
    {
        return false;
    }
}

if (!function_exists('carbon_get_comment_meta')) {
    function carbon_get_comment_meta($id, $name, $type = null)
    {
        return false;
    }
}