( function( $ ) {
	'use strict';

	function rgbaGetHex( rgba ) {
		var match = rgba.match( /^rgba\((\d+),\s?(\d+),\s?(\d+)/i );
		if ( match ) {
			return '#' +
				( '0' + parseInt( match[1], 10 ).toString( 16 ) ).slice( -2 ) +
				( '0' + parseInt( match[2], 10 ).toString( 16 ) ).slice( -2 ) +
				( '0' + parseInt( match[3], 10 ).toString( 16 ) ).slice( -2 );
		}
		return null;
	}

	function rgbaGetAlpha( rgba ) {
		var match = rgba.match( /^rgba\(\d+,\s?\d+,\s?\d+,\s?([\d\.]+)/i );
		if ( match ) {
			return Math.floor( 100 * parseFloat( match[1] ) );
		}
		return 100;
	}

	function hexAlphaToRgba( hex, alpha ) {
		var r, g, b, a;
		if ( ! hex ) {
			return '';
		}
		if ( hex.charAt( 0 ) === '#' ) {
			hex = hex.substring( 1 );
		}
		if ( typeof alpha === 'undefined' ) {
			alpha = 100;
		}
		r = parseInt( hex.substring( 0, 2 ), 16 );
		g = parseInt( hex.substring( 2, 4 ), 16 );
		b = parseInt( hex.substring( 4, 6 ), 16 );
		a = Math.max( 0, Math.min( 1, alpha / 100 ) );
		return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
	}

	// eslint-disable-next-line camelcase
	wp.customize.controlConstructor.color_rgba = wp.customize.Control.extend( {
		ready: function() {
			var control     = this;
			var updating    = false;
			var picker      = control.container.find( '.color-picker-hex' );
			var alphaSlider = control.container.find( '.alpha-slider' );
			var alphaNumber = control.container.find( '.alpha-number' );

			var updateSetting = function() {
				updating = true;
				control.setting.set( hexAlphaToRgba( picker.wpColorPicker( 'color' ), alphaSlider.val() ) );
				updating = false;
			};

			picker.val( rgbaGetHex( control.setting() ) ).wpColorPicker( {
				change: updateSetting,
				clear: updateSetting,
			} );

			alphaSlider.val( rgbaGetAlpha( control.setting() ) ).on( 'input', function() {
				alphaNumber.val( alphaSlider.val() );
				updateSetting();
			} );
			alphaNumber.val( rgbaGetAlpha( control.setting() ) ).on( 'input', function() {
				alphaSlider.val( alphaNumber.val() );
				updateSetting();
			} );

			control.setting.bind( function( value ) {
				var hex, alpha;
				// Bail if the update came from the control itself.
				if ( updating ) {
					return;
				}
				hex = rgbaGetHex( value );
				alpha = rgbaGetAlpha( value );
				picker.val( hex );
				picker.wpColorPicker( 'color', hex );
				alphaSlider.val( alpha );
				alphaNumber.val( alpha );
			} );

			// Collapse color picker when hitting Esc instead of collapsing the current section.
			control.container.on( 'keydown', function( event ) {
				var pickerContainer;
				if ( 27 !== event.which ) { // Esc.
					return;
				}
				pickerContainer = control.container.find( '.wp-picker-container' );
				if ( pickerContainer.hasClass( 'wp-picker-active' ) ) {
					picker.wpColorPicker( 'close' );
					control.container.find( '.wp-color-result' ).focus();
					event.stopPropagation(); // Prevent section from being collapsed.
				}
			} );
		},
	} );

	window.nur = window.nur || {};
	window.nur.customize = window.nur.customize || {};
	window.nur.customize.rgbaGetHex = rgbaGetHex;
	window.nur.customize.rgbaGetAlpha = rgbaGetAlpha;
	window.nur.customize.hexAlphaToRgba = hexAlphaToRgba;

} ( jQuery ) );
