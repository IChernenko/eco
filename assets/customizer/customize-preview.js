/**
 * Instantly live-update customizer settings in the preview for improved user experience.
 */

( function( $ ) {
	'use strict';

	// Site Branding Text visibility
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( 'body' ).addClass( 'hide-site-branding-text' );
			} else {
				$( 'body' ).removeClass( 'hide-site-branding-text' );
			}
		} );
	} );

	// Vertical padding around the logo
	wp.customize( 'logo_padding', function( value ) {
		value.bind( function( to ) {
			if ( ! to ) {
				return;
			}

			$( '.custom-logo' )
				.css( 'padding-top', to + 'px' )
				.css( 'padding-bottom', to + 'px' );
			$( '.mobile-menu-toggle' )
				.css( 'margin-top', ( to / 2 ) + 'px' );
		} );
	} );

	// Site Title color
	wp.customize( 'site_title_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-branding-text .site-title a' ).css( 'color', to );
		} );
	} );

	// Header background color
	wp.customize( 'header_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.page-header-inner' ).css( 'background-color', to );
		} );
	} );

	// Footer background color
	wp.customize( 'footer_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.footer-top-wrap' ).css( 'background-color', to );
		} );
	} );

	// Footer bottom background color
	wp.customize( 'footer_bottom_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.footer-bottom-wrap' ).css( 'background-color', to );
		} );
	} );

	// Custom colors
	$.each( [ 'primary_color', 'primary_color_darker' ], function( index, id ) {
		wp.customize( id, function( setting ) {
			setting.bind( function( to ) {
				var style = $( '#nur-custom-' + id );
				var currentColor = style.data( 'currentColor' );
				var css = style.html();

				css = css.split( ' ' + currentColor ).join( ' ' + to );
				style.html( css ).data( 'current-color', to );
			} );
		} );
	} );

	// Header Featured Content
	$.each( [ 1, 2, 3 ], function( index, id ) {
		var iconSel = '.js-header-featured-content-' + id + ' .header-featured-content-icon';

		wp.customize( 'header_featured_content_icon_class_' + id, function( value ) {
			value.bind( function( to ) {
				$( iconSel ).html( '<span class="' + to + '"></span>' );
			} );
		} );

		wp.customize( 'header_featured_content_icon_style_' + id, function( value ) {
			value.bind( function( to, from ) {
				$( iconSel )
					.removeClass( 'header-featured-content-icon--' + from )
					.addClass( 'header-featured-content-icon--' + to );
			} );
		} );
	} );

} ( jQuery ) );
