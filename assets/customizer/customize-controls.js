( function( api ) {
	'use strict';

	// Extend number input control to trigger value updates on 'enter' key press.
	api.controlConstructor.number = api.Control.extend( {
		ready: function() {
			var control = this;

			control.container.on( 'keydown', function( event ) {
				if ( 13 === event.which ) { // Enter.
					control.setting.set( control.container.find( 'input' ).val() );
				}
			} );
		},
	} );

} ( wp.customize ) );
