<?php /* Template Name: Library */ ?>

<?php get_header();

$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'library-page.php'
));
$page_id = $pages[0]->ID;

?>

    <div class="wrapper">
        <div class="container">

<!--            --><?php //while (have_posts()) : the_post(); ?>
<!--                --><?php //$download_items = carbon_get_post_meta($page_id, 'crb_slide', 'complex'); ?>

                <?php
                $download_items = carbon_get_post_meta($page_id, 'add_download_file', 'complex');


                foreach ($download_items as $download_item) : ?>

                    <div class="download-file">
<!--                        <a class="link-file" href="--><!--"></a>-->
                        <div class="download-img">
                            <img src="<?php echo wp_get_attachment_image_url($download_item['photo']) ?>" alt="image">
                        </div>
                        <!-- /.download-img -->
                        <div class="download-file-name">
                            <h3><?php echo $download_item['title']; ?></h3>
                            <p><?php echo $download_item['description']; ?></p>
                        </div>
                        <!-- /.download-file-name -->
                        <div class="download-btn">
                            <a class="btn btn-primary" href="<?php echo is_numeric($download_item['link_document'])? wp_get_attachment_url($download_item['link_document']) : $download_item['link_document']; ?>"><?php echo $download_item['link_btn']; ?></a>
                        </div>
                        <!-- /.download-btn -->
                    </div>

                <?php endforeach; ?>

<!--            --><?php //endwhile; ?>
        </div>
        <!-- /.container -->
    </div>

<?php get_footer(); ?>