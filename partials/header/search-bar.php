<?php
/**
 * Template for Search Bar displayed in the header.
 *
 * @package Nur
 */

if ( ! nur_show_header_search_bar() ) {
	return;
}
?>

<div class="site-header-search-bar">
	<?php get_search_form(); ?>
</div>
