<?php
/**
 * Displays header image, page title and breadcrumbs.
 *
 * @package Nur
 */

if ( ! nur_show_page_header() ) {
	return;
}
?>

<div class="page-header">

	<?php if ( has_header_image() ) : ?>
		<div class="page-header-image"><?php nur_header_image_tag(); ?></div>
	<?php endif; ?>

	<div class="page-header-inner">
		<div class="container">

			<?php if ( is_front_page() && is_home() ) : ?>
				<h2 class="page-title site-description"><?php echo esc_html( get_bloginfo( 'description', 'display' ) ); ?></h2>
			<?php else : ?>
				<h1 class="page-title"><?php nur_the_page_title(); ?></h1>
				<?php nur_breadcrumbs(); ?>
			<?php endif; ?>

		</div>
	</div>

</div>
