<?php
/**
 * Template for additional information displayed in the header.
 *
 * @package Nur
 */

if ( ! is_customize_preview() &&
	! nur_show_header_featured_content( 1 ) &&
	! nur_show_header_featured_content( 2 ) &&
	! nur_show_header_featured_content( 3 ) ) {
	return;
}
?>

<div class="site-header-featured-content">

	<div class="site-header-featured-content-column">
		<?php nur_header_featured_content( 1 ); ?>
	</div>

	<div class="site-header-featured-content-column">
		<?php nur_header_featured_content( 2 ); ?>
		<?php nur_header_featured_content( 3 ); ?>
	</div>

</div>
