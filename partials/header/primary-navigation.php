<?php
/**
 * Displays primary navigation menu.
 *
 * @package Nur
 */

if ( ! has_nav_menu( 'primary' ) ) {
	return;
}
?>

<div class="site-header-menu">

	<div class="container">

		<a class="mobile-menu-toggle" href="#site-navigation">
			<span class="screen-reader-text"><?php echo esc_html__( 'Toggle navigation menu', 'nur' ); ?></span>
			<span class="mobile-menu-toggle-bar mobile-menu-toggle-bar-1"> </span>
			<span class="mobile-menu-toggle-bar mobile-menu-toggle-bar-2"> </span>
			<span class="mobile-menu-toggle-bar mobile-menu-toggle-bar-3"> </span>
		</a>

		<nav id="site-navigation" class="primary-navigation" aria-label="<?php esc_attr_e( 'Navigation menu', 'nur' ); ?>">
			<div class="site-header-branding" itemscope itemtype="http://schema.org/Brand">

				<?php the_custom_logo(); ?>

				<?php nur_site_branding_text(); ?>

			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav>

	</div>

</div>
