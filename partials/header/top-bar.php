<?php
/**
 * Template of the top bar strip above the header.
 *
 * @package Nur
 */

if ( ! has_nav_menu( 'top-bar-1' ) && ! has_nav_menu( 'top-bar-2' ) ) {
	return;
}
?>

<div class="top-bar">
	<div class="container">

		<?php if ( has_nav_menu( 'top-bar-1' ) ) : ?>
			<nav class="secondary-navigation social-nav-links">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'top-bar-1',
					'depth'          => 2,
				) );
				?>
			</nav>
		<?php endif; ?>

		<?php if ( has_nav_menu( 'top-bar-2' ) ) : ?>
			<nav class="secondary-navigation social-nav-links">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'top-bar-2',
					'depth'          => 2,
				) );
				?>
			</nav>
		<?php endif; ?>

	</div>
</div>
