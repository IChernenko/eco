<?php
/**
 * Template part for displaying post previews.
 *
 * @package Nur
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry entry--preview' ); ?>>

	<?php if ( has_post_thumbnail() ) : ?>
		<a href="<?php the_permalink(); ?>" rel="bookmark">
			<div class="entry-image"><?php the_post_thumbnail( 'nur_featured_image' ); ?></div>
		</a>
	<?php endif; ?>

	<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

	<?php nur_post_meta(); ?>

	<div class="entry-excerpt"><?php the_excerpt(); ?></div>

	<a class="entry-more-link" href="<?php the_permalink(); ?>" rel="bookmark"><?php esc_html_e( 'Read more', 'nur' ); ?></a>

</article>
