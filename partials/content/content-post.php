<?php
/**
 * Template part for displaying blog post content.
 *
 * @package Nur
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>

	<?php if ( has_post_thumbnail() ) : ?>

		<div class="entry-image"><?php the_post_thumbnail( 'nur_featured_image' ); ?></div>

	<?php endif; ?>


	<?php if ( ! nur_show_page_header() ) : ?>

		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

	<?php endif; ?>


	<?php nur_post_meta(); ?>


	<div class="entry-content">

		<?php the_content(); ?>

	</div>


	<?php
	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'nur' ),
		'after'  => '</div>',
	) );
	?>

</article>
