<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @package Nur
 */

?>

<div class="no-results">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
		<p>
			<?php
			/* translators: %s is a link to create new post */
			printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'nur' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) );
			?>
		</p>
	<?php elseif ( is_search() ) : ?>
		<h2 class="no-results-title"><?php echo esc_html__( 'Sorry, no results were found for your query.', 'nur' ); ?></h2>
		<p><?php echo esc_html__( 'Please try again with some different keywords.', 'nur' ); ?></p>
	<?php else : ?>
		<p><?php echo esc_html__( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'nur' ); ?></p>
	<?php endif ?>
</div>
