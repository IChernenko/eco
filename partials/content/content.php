<?php
/**
 * Template part for displaying content of singluar pages.
 *
 * @package Nur
 */

?>

<div class="page-content">

	<?php the_content(); ?>

</div>

<?php
wp_link_pages( array(
	'before' => '<div class="page-links">' . esc_html__( 'Pages: ', 'nur' ),
	'after'  => '</div>',
) );
?>
